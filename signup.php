<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Courgette|Roboto|Pacifico'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/signup.css">
    <title>Signup</title>
</head>

<body>
    <?php
    session_start();
    if (empty($_SESSION['token'])) {
        $_SESSION['token'] = bin2hex(random_bytes(32));
    }
    $token = $_SESSION['token'];

?>
    <div class="signup-form">
        <form action="" method="post">
            <input type="hidden" name="csrf"
                value="<?php echo $token ?>">
            <div class="form-header">
                <h2>Sign up</h2>
                <p>Fill out this form and start chatting with your friends</p>
            </div>
            <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" name="user_name" placeholder="Example: laura" autocomplete="off"
                    required>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="user_pass" placeholder="password" autocomplete="off"
                    required>
            </div>
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" class="form-control" name="user_email" placeholder="someone@site.com"
                    autocomplete="off" required>
            </div>
            <div class="form-group">
                <label>Country</label>
                <select class="form-control" name="user_country" required>
                    <option disabled="">Select a Country</option>
                    <option>USA</option>
                    <option>Denmark</option>
                    <option>Italy</option>
                    <option>Spain</option>
                    <option>France</option>
                    <option>Germany</option>
                    <option>Sweden</option>
                    <option>Norway</option>
                </select>
            </div>
            <div class="form-group">
                <label>Gender</label>
                <select class="form-control" name="user_gender" required>
                    <option disabled="">Select your Gender</option>
                    <option>Male</option>
                    <option>Female</option>
                    <option>Other</option>
                </select>
            </div>
            <div class="for-group">
                <label class="checkbox-inline"><input type="checkbox" required> I accept the <a href="#">Terms of Use
                    </a>&amp; <a href="#">Privacy Policy</a></label>

            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-lg" name="sign_up">Sign Up</button>
            </div>
            <?php include("signup_user.php");?>
        </form>
        <div class="text-center small" style="color:#674288;">Already have an account? <a href="index.php">Log in</a>
        </div>
    </div>
</body>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"></script>

</html>