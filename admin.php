<!DOCTYPE html>
<?php session_start();
include("include/connection.php");

if (!isset($_SESSION['user_email']) && $_SESSION['user_type'] != 'admin') {
    header("Location: index.php");
} else {
    ?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Courgette|Roboto|Pacifico'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <title>ChatApp - Admin Portal</title>
</head>

<body>
    <?php include('include/navbar.php'); ?>
    <div class="header">
        <h2>Admin - Home Page</h2>
    </div>
    <div class="content">

        <!-- logged in user information -->
        <div class="profile_info">
            <img src="../images/profile_avatar.png">

            <div>
                <?php  if (isset($_SESSION['user_signed_in'])) : ?>
                <strong><?php echo ucfirst($_SESSION['user_signed_in']); ?></strong>

                <small>
                    <i style="color: #888;">(<?php echo ucfirst($_SESSION['user_type']); ?>)</i>
                    <br>
                    &nbsp; <a type="button" class="btn btn-info" href="create_user.php"> Add user</a>
                </small>

                <?php endif ?>
            </div>
        </div>
    </div>
</body>

</html>

<?php
}
