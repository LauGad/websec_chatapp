<!DOCTYPE html>
<?php session_start();
include("include/connection.php");

if (!isset($_SESSION['user_email'])) {
    header("Location: index.php");
} else {
    ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Courgette|Roboto|Pacifico'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <title>Change Profile Picture</title>
</head>

<body>

    <?php


    include('include/navbar.php');
    
    $user = $_SESSION['user_signed_in'];

    $stmt = $con->prepare("SELECT * FROM users WHERE user_name=?");
    $stmt->bind_param("s", $user);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_array(MYSQLI_ASSOC);

    $user_profile = $row['user_profile'];

    echo"
    <div class='card'>
        <img src='$user_profile'>
        <h1>$user</h1>
        <form method='POST' enctype='multipart/form-data'>
            <label id='update_profile'><i class='fa fa-circle-o' aria-hidden='true'></i>Select Profile
            <input type='file' name='u_image' size='60'>
            </label>
            <button id='button_profile' name='update'>&nbsp&nbsp&nbsp <i class='fa fa-heart' aria-hidden='true'>Update Profile</i></button>
        </form>
    </div>
    ";
    
    
    if (isset($_POST["update"])) {
        try {
   
    // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
            !isset($_FILES['u_image']['error']) ||
            is_array($_FILES['u_image']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['u_image']['error'] value.
            switch ($_FILES['u_image']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // Check filesize
            if ($_FILES['u_image']['size'] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            // Check MIME Type
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                $finfo->file($_FILES['u_image']['tmp_name']),
                array(
                    'jpg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif',
                ),
                true
            )) {
                throw new RuntimeException('Invalid file format.');
            }

            // Name it uniquely
            if (!move_uploaded_file(
                $_FILES['u_image']['tmp_name'],
                $img = sprintf(
                    'images/%s.%s',
                    sha1_file($_FILES['u_image']['tmp_name']),
                    $ext
                )
            )) {
                throw new RuntimeException('Failed to move uploaded file.');
            }
            
            $stmt = $con->prepare("UPDATE users SET user_profile = ? WHERE user_name=?");
            $stmt->bind_param("ss", $img, $user);
            $stmt->execute();

            if ($stmt) {
                echo"<script>alert('Your profile picture was updated')</script>";
                echo"<script>window.open('upload.php', '_self')</script>";
            }
        } catch (RuntimeException $e) {
            echo $e->getMessage();
        }
    } ?>



</body>

</html>
<?php
}
