<?php
include("include/connection.php");


    if (isset($_POST['sign_up'])) {
        if (hash_equals($token, $_POST['csrf'])) {
            $name = htmlentities(mysqli_real_escape_string($con, $_POST['user_name']));
            $pass = htmlentities(mysqli_real_escape_string($con, $_POST['user_pass']));
            $email = htmlentities(mysqli_real_escape_string($con, $_POST['user_email']));
            $country = htmlentities(mysqli_real_escape_string($con, $_POST['user_country']));
            $gender = htmlentities(mysqli_real_escape_string($con, $_POST['user_gender']));
            print($email);
            if ($name== '') {
                echo"<script>alert('We cannot verify your name')</script>";
                exit();
            }
            if (strlen($pass)<8) {
                echo"<script>alert('Password should be minimum 8 characters')</script>";
                exit();
            }

            $stmt = $con->prepare("SELECT * FROM users WHERE user_email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $result = $stmt->get_result();

            $check = mysqli_num_rows($result);

            if ($check==1) {
                echo"<script>alert('Email already exists. Please try again.')</script>";
                echo"<script>window.open('signup.php', '_self')</script>";
                exit();
            }

            $profile_pic = "images/profile_avatar.png";
            $pass = password_hash($pass, PASSWORD_DEFAULT);

            $stmt = $con->prepare("insert into users(user_name, user_pass, user_email, user_profile, user_country, user_gender, log_in, user_type)values(?,?,?,?,?,?,?,?)");
            $log_in = 'Offline';

            // check if e-mail address is well-formed
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                //admin is creating a user
                if (isset($_POST['user_type']) && $_POST['user_type'] != '') {
                    $stmt->bind_param("ssssssss", $name, $pass, $email, $profile_pic, $country, $gender, $log_in, $_POST['user_type']);
                //user is signing up
                } else {
                    $user_type = 'user';
                    $stmt->bind_param("ssssssss", $name, $pass, $email, $profile_pic, $country, $gender, $log_in, $user_type);
                }
                $stmt->execute();
                if ($stmt) {
                    echo"<script>alert('Congratulations $name, your account has been created successfully.')</script>";
                    echo"<script>window.open('index.php', '_self')</script>";
                } else {
                    echo"<script>alert('Registartion failed. Please try again.')</script>";
                    echo"<script>window.open('signup.php', '_self')</script>";
                }
            } else {
                echo"<script>alert('Email is invalid. Please try again.')</script>";
                echo"<script>window.open('signup.php', '_self')</script>";
            }
        }
    }
