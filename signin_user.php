<?php
session_start();

include("include/connection.php");


if (isset($_POST['sign_in'])) {
    if (hash_equals($token, $_POST['csrf'])) {
        $email = htmlentities(mysqli_real_escape_string($con, $_POST['email']));
        $pass = htmlentities(mysqli_real_escape_string($con, $_POST['pass']));

        $stmt = $con->prepare("SELECT * FROM users WHERE user_email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $row = $result->fetch_array(MYSQLI_ASSOC);

        $check_user = mysqli_num_rows($result);

        if ($check_user == 1) {
            //password is valid
            if (password_verify($pass, $row['user_pass'])) {
                $_SESSION['user_email']=$email;
                $stmt = $con->prepare("UPDATE users SET log_in='Online' WHERE user_email=?");
                $stmt->bind_param("s", $email);
                $stmt->execute();
    
                $stmt = $con->prepare("SELECT * FROM users WHERE user_email=?");
                $stmt->bind_param("s", $email);
                $stmt->execute();
                $result = $stmt->get_result();
                $row = $result->fetch_array(MYSQLI_ASSOC);

                $user_name = $row['user_name'];
                $user_type = $row['user_type'];
                $_SESSION['user_signed_in']=$user_name;
                $_SESSION['user_type']=$user_type;

                
                if ($user_type == 'admin') {
                    header('Location: admin.php');
                } else {
                    echo "<script>window.open('home.php?user_name=$user_name', '_self')</script>";
                }
            }
            //password is invalid
            else {
                //increase nr of login-attempts
                $attempts = $row['failed_logins']+1;
                if ($attempts < 3) {
                    $stmt = $con->prepare("UPDATE users SET failed_logins=? WHERE user_email=?");
                    $stmt->bind_param("ss", $attempts, $email);
                    $stmt->execute();
                    $tries = 3 - $attempts;
                    if ($tries == 1) {
                        echo"
                    <div class='alert alert-danger'>
                        <strong>The password is incorrect. You have 1 attempt left.</strong>
                    </div>

                    ";
                    } else {
                        echo"
                        <div class='alert alert-danger'>
                            <strong>The password is incorrect. You have $tries attempts left.</strong>
                        </div>

                        ";
                    }
                    exit();
                //too many attempts
                } else {
                    //lock account
                    if (is_null($row['lockout_time'])) {
                        $stop_time = date('Y-m-d h:i:s', strtotime($stop_time . ' +5 minutes'));
                        $stmt = $con->prepare("UPDATE users SET lockout_time=? WHERE user_email=?");
                        $stmt->bind_param("ss", $stop_time, $email);
                        $stmt->execute();
                    }
                    //already locked
                    else {
                        //echo "locked until ".$row['lockout_time']."<br>";
                        //echo "now is ".date("Y-m-d h:i:s")."<br>";
                        if ($row['lockout_time']<date("Y-m-d h:i:s")) {
                            //time ended
                            $stmt = $con->prepare("UPDATE users SET failed_logins=0, lockout_time=null WHERE user_email=?");
                            $stmt->bind_param("s", $email);
                            $stmt->execute();
                        }
                    }
                }

                echo"
                <div class='alert alert-danger'>
                    <strong>Your account has been temporarily locked. Please try again in a few minutes.</strong>
                </div>

                ";
            }
            //email doesn't exist in the database
        } else {
            echo"
        <div class='alert alert-danger'>
            <strong>We don't have an account registered with this email address.</strong>
        </div>

        ";
        }
    }
}
