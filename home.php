<!DOCTYPE html>
<?php session_start();
include("include/connection.php");

if (!isset($_SESSION['user_email'])) {
    header("Location: index.php");
} else {
    ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Courgette|Roboto|Pacifico'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <title>ChatApp - HOME</title>
</head>

<body>
    <?php include('include/navbar.php');
    
    session_start();
    if (empty($_SESSION['token'])) {
        $_SESSION['token'] = bin2hex(random_bytes(32));
    }
    $token = $_SESSION['token']; ?>
    <div class="container main section">

        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12 left-sidebar">
                <!-- <div class="input-group searchbox">
                    <div class="input-group-btn">
                        <center><a href="include/find_friends.php"><button class="btn btn-dafault search-icon"
                                    name="search_user" type="submit">Add new user</button></a></center>
                    </div>
                </div> -->
                <div class="left-chat">
                    <h6>Contacts</h6>
                    <ul>
                        <?php include("include/get_users_data.php"); ?>
                    </ul>
                </div>
            </div>
            <div class="col-mid-9 col-sm-9 col-xs-12 right-sidebar">
                <div class="row">
                    <!-- get info of logged in user -->
                    <?php
                        $user_signed_in = $_SESSION['user_signed_in']; ?>
                    <!-- get user data on which user clicks -->
                    <?php
                        if (isset($_GET['user_name'])) {
                            global $con;
                            
                            $username = htmlentities($_GET['user_name']);

                            $stmt = $con->prepare("SELECT * FROM users WHERE user_name = ?");
                            $stmt->bind_param("s", $username);
                            $stmt->execute();
                            $result = $stmt->get_result();
                            $row = $result->fetch_array(MYSQLI_ASSOC);

                            // $username = $row_user['user_name'];
                            $user_profile_image = $row['user_profile'];

                            //mark messages read from user we clicked on
                            $stmt = $con->prepare("UPDATE users_chat SET msg_status='read' WHERE receiver_username=? AND sender_username=?");
                            $stmt->bind_param("ss", $user_signed_in, $username);
                            $stmt->execute();
                        }

    $stmt = $con->prepare("SELECT * FROM users_chat WHERE (sender_username = ? AND receiver_username = ? OR(receiver_username = ? AND sender_username = ?))");
    $stmt->bind_param("ssss", $user_signed_in, $username, $user_signed_in, $username);
    $stmt->execute();
    $result = $stmt->get_result();
    $total = mysqli_num_rows($result); ?>
                    <div class="col-md-12 right-header">
                        <div class="right-header-img">
                            <img src=<?php echo"$user_profile_image"; ?>>
                        </div>
                        <div class="right-header-detail">
                            <p><?php echo "$username"; ?>
                            </p>
                            <span><?php echo $total; ?>
                                messages</span>
                            &nbsp &nbsp

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="scrolling_to_bottom" class="col-md-12 right-header-contentChat">
                        <?php
    $stmt = $con->prepare("SELECT * FROM users_chat WHERE (sender_username =? AND receiver_username=?) OR (receiver_username=? AND sender_username=?) ORDER BY 1 ASC");
    $stmt->bind_param("ssss", $user_signed_in, $username, $user_signed_in, $username);
    $stmt->execute();
    $result = $stmt->get_result();
    
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $sender_username = $row['sender_username'];
        $receiver_username = $row['receiver_username'];
        $msg_content = $row['msg_content'];
        $msg_date = $row['msg_date']; ?>
                        <ul>
                            <?php
                                if ($user_signed_in == $sender_username and $username == $receiver_username) {
                                    echo"
                                        <li>
                                            <div class='rightside-right-chat'>
                                                <span>$sender_username <small>$msg_date</small></span><br><br>
                                                <p>$msg_content</p>
                                            </div>
                                        </li>
                                    ";
                                } elseif ($user_signed_in == $receiver_username and $username == $sender_username) {
                                    echo"
                                        <li>
                                            <div class='rightside-left-chat'>
                                                <span>$sender_username <small>$msg_date</small></span><br><br>
                                                <p>$msg_content</p>
                                            </div>
                                        </li>
                                    ";
                                } ?>
                        </ul>
                        <?php
    } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 right-chat-textbox">
                        <form method="POST">
                            <input type="hidden" name="csrf"
                                value="<?php echo $token ?>">
                            <input autocomplete="off" type="text" name="msg_content"
                                placeholder="Write your message.....">
                            <button class="btn" name="submit"><i class="fa fa-telegram" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
        if (isset($_POST['submit'])) {
            if (hash_equals($token, $_POST['csrf'])) {
                $msg = htmlentities($_POST['msg_content']);
                if ($msg == "") {
                    echo"
                    <div class='alert alert-danger'>
                        <strong><center>Message was unable to send</center></strong>
                    </div>
                ";
                } elseif (strlen($msg) > 100) {
                    echo"
                    <div class='alert alert-danger'>
                        <strong><center>Message is too long. Use only 100 characters</center></strong>
                    </div>
                ";
                } else {
                    $stmt = $con->prepare("INSERT INTO users_chat(sender_username, receiver_username, msg_content, msg_status, msg_date) VALUES(?, ?, ?, 'unread', NOW())");
                    $stmt->bind_param("sss", $user_signed_in, $username, $msg);
                    $stmt->execute();
                    echo"<script>window.open('home.php?user_name=$username', '_self')</script>";
                }
            }
        } ?>

    <script>
        $('#scrolling_to_bottom').animate({
            scrollTop: $('#scrolling_to_bottom').get(0).scrollHeight
        }, 100);
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            var height = $(window).height();
            $('.left-chat').css('height', (height - 63) + 'px');
            $('.right-header-contentChat').css('height', (height - 225) + 'px');
        });
    </script>

</body>

</html>

<?php
}
