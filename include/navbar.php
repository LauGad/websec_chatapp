<?php
    session_start();
    if (empty($_SESSION['token'])) {
        $_SESSION['token'] = bin2hex(random_bytes(32));
    }
    $token = $_SESSION['token'];

?>
<nav class="navbar navbar-expand-sm bg-light navbar-light ">

    <a class="navbar-brand" href="#">
        <?php
                $user = $_SESSION['user_signed_in'];
    if ($_SESSION['user_type'] == 'admin') {
        echo"<a class='navbar-brand' href='admin.php'>ChatApp</a>";
    } else {
        echo"<a class='navbar-brand' href='home.php?user_name=$user'>ChatApp</a>";
    } ?>
    </a>
    <ul class="navbar-nav mr-auto">
        <li><a class="nav-link" href="account_settings.php">Settings</a></li>
    </ul>
    <form method="POST">
        <input type="hidden" name="csrf"
            value="<?php echo $token ?>">
        <span class="navbar-text">Welcome, <?php echo ucfirst($user)?>
        </span>
        <button name="logout" class="btn btn-danger">Logout</button>
    </form>
    <?php

                if (isset($_POST['logout'])) {
                    if (hash_equals($token, $_POST['csrf'])) {
                        $stmt = $con->prepare("UPDATE users SET log_in='Offline' WHERE user_name=?");
                        $stmt->bind_param("s", $user);
                        $stmt->execute();

                        header("Location: logout.php");
                        exit();
                    }
                } ?>
</nav>