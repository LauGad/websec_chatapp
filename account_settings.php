<!DOCTYPE html>
<?php session_start();
include("include/connection.php");

if (!isset($_SESSION['user_email'])) {
    header("Location: index.php");
} else {
    ?>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Courgette|Roboto|Pacifico'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/account_settings.css">


    <title>Account Settings</title>
</head>

<body>

    <?php include('include/navbar.php');
    
    session_start();
    if (empty($_SESSION['token'])) {
        $_SESSION['token'] = bin2hex(random_bytes(32));
    }
    $token = $_SESSION['token']; ?>

    <div class="row">
        <div class="col-sm-2">

        </div>
        <?php
            $user_email = $_SESSION['user_email'];
    $stmt = $con->prepare("SELECT * FROM users WHERE user_email = ?");
    $stmt->bind_param("s", $user_email);
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_array(MYSQLI_ASSOC);
                            
    $user_id = $row['user_id'];
    $user_name = $row['user_name'];
    $user_email = $row['user_email'];
    // $user_pass = $row['user_pass'];
    $user_profile = $row['user_profile'];
    $user_country = $row['user_country'];
    $user_gender = $row['user_gender']; ?>
        <div class="col-sm-8">
            <form method="POST" enctype="multipart/formdata">
                <input type="hidden" name="csrf"
                    value="<?php echo $token ?>">
                <table class="table table-bordered table-hover">
                    <tr align="center">
                        <td colspan="6" class="active">
                            <h2>Change Account Settings</h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="bold-text">Username</td>
                        <td>
                            <input type="text" name="u_name" class="form-control" required
                                value="<?php echo $user_name; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><a class="btn btn-detail" href="upload.php"><i class="fa fa-user" aria-hidden="true"></i>
                                Change Profile Picture</a></td>
                    </tr>
                    <tr>
                        <td class="bold-text">Email</td>
                        <td>
                            <input type="email" name="u_email" class="form-control" required
                                value="<?php echo $user_email; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td class="bold-text">Country</td>
                        <td>
                            <select class="form-control" name="u_country">
                                <option><?php echo $user_country ?>
                                </option>
                                <option>USA</option>
                                <option>Denmark</option>
                                <option>Italy</option>
                                <option>Spain</option>
                                <option>France</option>
                                <option>Germany</option>
                                <option>Sweden</option>
                                <option>Norway</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="bold-text">Gender</td>
                        <td>
                            <select class="form-control" name="u_gender">
                                <option><?php echo $user_gender ?>
                                </option>
                                <option>Male</option>
                                <option>Female</option>
                                <option>Other</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal"><i
                                    class="fa fa-shield"></i> Security Question</button>
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form
                                                action="recovery.php?id=<?php echo $user_id?>"
                                                method="POST" id="form">
                                                <strong>What is your mother's maiden name?</strong>
                                                <textarea class="form-control" cols="83" rows="4" name="content"
                                                    placeholder="Someone"></textarea><br>
                                                <input type="submit" class="btn btn-info" name="sub"
                                                    value="Submit"><br><br>
                                                <p>We will ask you this question if you forget your password.</p>
                                                <br><br>
                                            </form>
                                            <?php
                                                if (isset($_POST['sub'])) {
                                                    if (hash_equals($token, $_POST['csrf'])) {
                                                        $bfn = htmlentities($_POST['content']);
                                                        if ($bfn == '') {
                                                            echo"<script>alert('Please enter something.') </script>";
                                                            echo"<script>window.open('acccount_settings.php', '_self')</script>";
                                                            exit();
                                                        } else {
                                                            $stmt = $con->prepare("UPDATE users SET forgotten_answer=? WHERE user_email = ?");
                                                            $stmt->bind_param("ss", $bfn, $user_email);
                                                            $stmt->execute();

                                                            if ($stmt) {
                                                                echo"<script>alert('Your answer was saved.') </script>";
                                                                echo"<script>window.open('account_settings.php', '_self')</script>";
                                                            } else {
                                                                echo"<script>alert('Error while updating information.') </script>";
                                                                echo"<script>window.open('account_settings.php', '_self')</script>";
                                                            }
                                                        }
                                                    }
                                                } ?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </td>

                    </tr>
                    <tr>
                        <td></td>
                        <td><a class="btn btn-default link" href="change_password.php"><i class="fa fa-key fa-fw"
                                    aria-hidden="true"></i> Change Password</a></td>
                    </tr>
                    <tr align="center">
                        <td colspan="6">
                            <input type="submit" value="Update" name="update" class="btn btn-info">
                        </td>
                    </tr>
                </table>
            </form>
            <?php
                if (isset($_POST['update'])) {
                    if (hash_equals($token, $_POST['csrf'])) {
                        $name = htmlentities($_POST['u_name']);
                        $email = htmlentities($_POST['u_email']);
                        $country = htmlentities($_POST['u_country']);
                        $gender = htmlentities($_POST['u_gender']);

                        $_SESSION['user_signed_in']=$name;
                        $_SESSION['user_email']=$email;
                    
                        $stmt = $con->prepare("UPDATE users SET user_name=?, user_email=?, user_country=?, user_gender=? WHERE user_email=?");
                        $stmt->bind_param("sssss", $name, $email, $country, $gender, $user_email);
                        $stmt->execute();

                        if ($stmt) {
                            echo"<script>window.open('account_settings.php', '_self')</script>";
                        }
                    }
                } ?>
        </div>
        <div class="col-sm-2">

        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

</body>

</html>
<?php
}
